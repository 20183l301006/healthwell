import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {NgxPermissionsService} from 'ngx-permissions';
import * as _ from 'lodash';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class NgxGuardGuard implements CanActivate {
//  private readonly USER_DATA_KEY = 'USER_DATA';
rol : any;
  constructor(private Auth:LoginService, private router: Router) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
   if (localStorage.getItem('token') == null) {
    //cada que sea nulo me lo redirija a login
    this.router.navigateByUrl('/login');
    return false;
   } else {
    return true;
   }
  }
  }