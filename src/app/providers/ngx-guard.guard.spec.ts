import { TestBed } from '@angular/core/testing';

import { NgxGuardGuard } from './ngx-guard.guard';

describe('NgxGuardGuard', () => {
  let guard: NgxGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(NgxGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
