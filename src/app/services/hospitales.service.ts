import { Hospitals } from './../models/hospitales';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class HospitalesService {

  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient, private token: LoginService) { }

  list(){
    return this.http.get(`${this.url}hospitals?token=${this.token.leer_token()}`);
  }

  insert(hospitals: any ) {
    return this.http.post(`${this.url}hospitals?token=${this.token.leer_token()}`, hospitals);
  }
  edit(uuid: string ){
    return this.http.get(`${this.url}hospitals/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}hospitals/${uuid}?token=${this.token.leer_token()}`);

  }

  Update( hospitals: Hospitals){
    return this.http.put(`${this.url}hospitals/${hospitals.uuid}?token=${this.token.leer_token()}`, hospitals);

  }
  CountHospitals(){
    return this.http.get(`${this.url}hospitals/count?token=${this.token.leer_token()}`);
  }


}
