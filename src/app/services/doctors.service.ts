import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient, private token: LoginService) { }
  list(){
    return this.http.get(`${this.url}doctors?token=${this.token.leer_token()}`);
  }
  list_Bloquers(){
    return this.http.get(`${this.url}doctors/delete?token=${this.token.leer_token()}`);
  }
  insert(admin: any ) {
    return this.http.post(`${this.url}doctors?token=${this.token.leer_token()}`, admin);
  }

  edit(uuid: string ){
    return this.http.get(`${this.url}doctors/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}doctors/${uuid}?token=${this.token.leer_token()}`);

  }
  return_datos(uuid:string){
    return this.http.get(`${this.url}return/${uuid}?token=${this.token.leer_token()}`);

  }
  Update( users: any){
    return this.http.put(`${this.url}doctors/${users.uuid}?token=${this.token.leer_token()}`, users);

  }
  Doctors_Count(){
    return this.http.get(`${this.url}doctors/count?token=${this.token.leer_token()}`);
  }

}
