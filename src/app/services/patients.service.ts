import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {
  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient, private token:LoginService) { }
  list(){
    return this.http.get(`${this.url}patients?token=${this.token.leer_token()}`);
  }
  insert(admin: any ) {
    return this.http.post(`${this.url}patients?token=${this.token.leer_token()}`, admin);
  }

  edit(uuid: string ){
    return this.http.get(`${this.url}patients/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}patients/${uuid}?token=${this.token.leer_token()}`);

  }
  restore(uuid:string){
    return this.http.get(`${this.url}restore/${uuid}?token=${this.token.leer_token()}`);

  }
  Update( users: any){
    return this.http.put(`${this.url}patients/${users.uuid}?token=${this.token.leer_token()}`, users);

  }
PatienTratamient(){
  return this.http.get(`${this.url}inquiries/count?token=${this.token.leer_token()}`);

}
Count_Expedients(){
  return this.http.get(`${this.url}patients/count?token=${this.token.leer_token()}`);

}
list_Bloquers(){
  return this.http.get(`${this.url}patients/bloquers?token=${this.token.leer_token()}`);
}
  }
