import { Recipe } from './../models/recipe';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient, private token: LoginService) { }
  list(){
    return this.http.get(`${this.url}recipe?token=${this.token.leer_token()}`);
  }

  insert(inquiries: any ) {
    return this.http.post(`${this.url}recipe?token=${this.token.leer_token()}`, inquiries);
  }
  edit(uuid: string ){
    return this.http.get(`${this.url}recipe/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}recipe/${uuid}?token=${this.token.leer_token()}`);

  }

  Update( recipe: Recipe){
    return this.http.put(`${this.url}recipe/${recipe.uuid}?token=${this.token.leer_token()}`, recipe);

  }
  returnRecipe(uuid:any){
    window.open(`${this.url}recipe/pdf/${uuid}?token=${this.token.leer_token()}`);
  }
  DownloadPdf(uuid:any){
    window.open(`${this.url}recipe/download/${uuid}?token=${this.token.leer_token()}`);
  }
}
