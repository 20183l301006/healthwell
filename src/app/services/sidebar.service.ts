import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any[] = [
    {
      titulo: 'Panel',
      icono: 'mdi mdi-gauge',
      submenu: [
        { titulo: 'Inicio', url: '/' },
        { titulo: 'Nuevo paciente', url: 'paciente' },
        { titulo: 'Agendar cita', url: 'citas' },

      ]
    },
    {
      id:2,
      titulo: 'Administracion',
      icono: 'fa fa-address-card',
      submenu: [
        { titulo: 'Administradores', url: 'administrativo' },
        { titulo: 'Doctores', url: 'doctores' },
        { titulo: 'Pacientes', url: 'admin-pacientes' },

        { titulo: 'Hospitales', url: 'hospitales' },

      ]
    }
  ];

  constructor() { }
}
