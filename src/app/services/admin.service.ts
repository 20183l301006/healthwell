import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Admin } from '../models/admin';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient, private token: LoginService) { }

  list(){
    return this.http.get(`${this.url}users?token=${this.token.leer_token()}`);
  }
  insert(admin: any ) {
    return this.http.post(`${this.url}users?token=${this.token.leer_token()}`, admin);
  }
  edit(uuid: string ){
    return this.http.get(`${this.url}users/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}users/${uuid}?token=${this.token.leer_token()}`);

  }

  Update( admin: Admin){
    return this.http.put(`${this.url}users/${admin.uuid}?token=${this.token.leer_token()}`, admin);

  }
}
