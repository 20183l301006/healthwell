import { Inquiries } from './../models/inquiries';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class InquiriesService {
  url = 'http://127.0.0.1:8000/api/';

  constructor(private http: HttpClient,private token: LoginService) { }
  list(){
    return this.http.get(`${this.url}inquiries?token=${this.token.leer_token()}`);
  }

  insert(inquiries: any ) {
    return this.http.post(`${this.url}inquiries?token=${this.token.leer_token()}`, inquiries);
  }
  edit(uuid: string ){
    return this.http.get(`${this.url}inquiries/${uuid}?token=${this.token.leer_token()}`);

  }

  delete(uuid:string){
    return this.http.delete(`${this.url}inquiries/${uuid}?token=${this.token.leer_token()}`);

  }

  Update( inquiries: Inquiries){
    return this.http.put(`${this.url}hospitals/${inquiries.uuid}?token=${this.token.leer_token()}`, inquiries);

  }

}
