import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Auth } from '../models/Auth.module';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://127.0.0.1:8000/api/';
  name:string = null;
  token: string = null;
  email: string = null;
  //uuid: string = null;
  //roles_id:string = null;
  rol:string;
  daots: false;
  constructor(private http: HttpClient, private router: Router) { }


  login(datos: Auth ) {
    return this.http.post(`${ this.url }login`, datos);
  }

guardar_token(idtoken: any){
  localStorage.setItem('token',idtoken);

}

leer_token (){
  if (localStorage.getItem('token')) {
    this.token = localStorage.getItem('token');
  }else{
    this.token = null;
  }
  return this.token;
}
guardar_nombre(name: string){
  this.name = name;
  localStorage.setItem('name',name);

}

leer_nombre (){
  if (localStorage.getItem('name')) {
    this.name = localStorage.getItem('name');
  }else{
    this.name = null;
  }
  return this.name;
}
guardar_email(email: string){
  this.email = email;
  localStorage.setItem('email',email);

}

leer_email(){
  if (localStorage.getItem('email')) {
    this.email = localStorage.getItem('email');
  }else{
    this.email = null;
  }
  return this.email;
}
logout(){
  localStorage.removeItem('token');
  localStorage.removeItem('name');
  localStorage.removeItem('email');
  localStorage.removeItem('rol');

  this.router.navigateByUrl('/login')
}

guardar_rol(name: string){
  this.rol = name;
  localStorage.setItem('rol',name);

}
leer_rol(){
  if (localStorage.getItem('rol')) {
    this.email = localStorage.getItem('rol');
  }else{
    this.email = null;
  }
  return this.email;
}
}

