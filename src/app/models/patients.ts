export class Patients{
  uuid: string;
living_place: string;
blood_type: string;
disability: string;
ethnic_group: string;
religion: string;
socioeconomic_level: string;
age: number;
hospitals_id:number;
persons_id:number;
name:string;
ap_patern: string;
ap_matern:string;
curp: string;
cell_phone: string;
telefone: string;
photo: string;
person:string
rol:string
email: string;
password: string;
password_confirmation: string;
patients_id: string;
inquiries_id: string;
receta:[];

constructor(){
  this.hospitals_id = 0;
  this.persons_id = 0;
}
}
