export class Admin{
    id_: number;
    uuid: string;
    name: string;
    ap_patern: string;
    ap_matern: string;
    curp: string;
    cell_phone: string;
    telefone:string;
    email: string;
    password: string;
    password_confirmation: string;
    token: string;
}