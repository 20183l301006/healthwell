export class Doctors{
    id_: number;
    uuid: string;
    name: string;
    ap_patern: string;
    ap_matern: string;
    curp: string;
    cell_phone: string;
    telefone:string;
    email: string;
    password: string;
    password_confirmation: string;
    id_card: String;
    specialty: string;
    sub_especialty: string;
    consulting_room: string;
    hospitals_id: number;
    token: string;
    constructor(){
        this.hospitals_id = 0;
    }
}
