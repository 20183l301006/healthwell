export class Inquiries{
  _id: number;
  uuid:string;
  num_inquirie: string;
  tratamiento: boolean;
  patients_id: number;
  doctors_id: number;

  constructor(){
    this.patients_id = 0;
    this.doctors_id = 0;
  }
}
