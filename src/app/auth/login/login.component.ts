import { Auth } from './../../models/Auth.module';
import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { SocialAuthService } from "angularx-social-login";
import { GoogleLoginProvider } from "angularx-social-login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
  //dato = true;
  name: string;
  token: any;
  reloagpage:any;
  auth: Auth = new Auth();


  constructor( private router: Router, private Login: LoginService,private authService:SocialAuthService) { }
  googleLoginOptions = {
    scope: 
      'profile email',
  
  };
  ngOnInit(){

  }

  login( form: NgForm) {
  if (form.invalid) {
  Swal.fire({
  position:'top-end',
  icon:'error',
  title:'Ha ocurrido un error',
  text:'Favor de verificar los datos ingresados',
  timer:1500,
});
  console.log('Formulario no valido');
  return;
}
  this.Login.login(this.auth).subscribe(
  res => {
  console.log(res);
//this.dato = false;
  this.Login.guardar_token(res['token']);
  this.Login.guardar_nombre(res['users']['name']);
  this.Login.guardar_email(res['users']['email']);
  this.Login.guardar_rol(res['users']['roles']['name']);


  Swal.close();
  Swal.fire({
  icon:'success',
  title:'Iniciaste sesión correctamente',
  text:`Bienvenido (a):`+res['users']['name']
  });
  this.router.navigateByUrl('');
  });
  }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID,this.googleLoginOptions).then(res => {
      console.log(res);
    })
    .catch(err => console.error(err));
  }
}
