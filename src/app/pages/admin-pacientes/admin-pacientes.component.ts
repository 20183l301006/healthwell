import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Patients } from 'src/app/models/patients';
import { HospitalesService } from 'src/app/services/hospitales.service';
import { PatientsService } from 'src/app/services/patients.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin-pacientes',
  templateUrl: './admin-pacientes.component.html',
  styleUrls: ['./admin-pacientes.component.css']
})
export class AdminPacientesComponent implements OnInit {
cargando: boolean = false;
patients: any;
cargando2: boolean = false;
patients2: any;
dato: boolean = false;
registro: Patients = new Patients();
hospitals:any;
  constructor(private patientsService:PatientsService, private hospitalService: HospitalesService) { }

  ngOnInit(): void {
    this.listPatients();
    this.listPatients_Bloquers();
    this.listHospitals();
  }
listPatients(){
  this.cargando = true;
  this.patientsService.list().subscribe(
    resp =>{
      this.patients = resp;
      this.cargando = false;
      console.log(this.patients);
    }
  );
}
listPatients_Bloquers(){
  this.cargando2 = true;
  this.patientsService.list_Bloquers().subscribe(
    respuesta => {
      this.patients2 = respuesta;
      this.cargando2 = false;
      console.log(this.patients2);

    }
  );
}
listHospitals(){
  this.hospitalService.list().subscribe(
    resp =>{
      this.hospitals = resp;
    }
  );
}
clickTrue(event){
  this.dato = event;
}
clickFalse(){
  this.dato = false;
}
guardar(from: NgForm) {
  if (from.invalid) {
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: 'Ha ocurrido un error',
      text: 'Favor de verificar los datos ingresados',
      timer: 1500,
    });
    console.log('Formulario no valido');
    return;
  }
  if (this.registro.uuid) {
    this.patientsService.Update(this.registro).subscribe(
      resp => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Se actualizo correctamente',
          showConfirmButton: false,
          timer: 1500,
        });
        this.listPatients();
        from.reset();
      });
  }
  else {
    this.patientsService.insert(this.registro).subscribe(
      resp => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'El paciente se ha registrado correctamente',
          showConfirmButton: false,
          timer: 1500,
        });
        this.listPatients();
        from.reset();

      });
  }    
}
borrar(patients: Patients, i: number) {
  Swal.fire({
    title: '¿Estas seguro que quieres bloquear a este usuario?',
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: 'success',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then((result) => {
    if (result.value) {
      console.log(patients.uuid);
      this.patientsService.delete(patients.uuid).subscribe(
        resp => {
          this.patients.splice(i, 1);
          this.listPatients_Bloquers();

        }
      )
    }
  })
}
desbloquear(patients2: Patients, j: number) {
  Swal.fire({
    title: '¿Estas seguro que quieres desbloquear a este usuario?',
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: 'success',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then((result) => {
    if (result.value) {
      console.log(patients2.uuid);
      this.patientsService.restore(patients2.uuid).subscribe(
        resp => {
          this.patients2.splice(j, 1);
          this.listPatients();

        }
      )
    }
  })
}
}
