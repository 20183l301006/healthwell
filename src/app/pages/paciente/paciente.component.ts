import { Component, OnInit } from '@angular/core';
import { PatientsService } from 'src/app/services/patients.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.css']
})
export class PacienteComponent implements OnInit {
  filtrarcurp = '';
patients:any;
  constructor(private patientsService:PatientsService) { }

  ngOnInit(): void {
this.patientsService.list().subscribe(
  resp =>{
    this.patients = resp;
  }
);  
  }

}
