import { RecipeService } from './../../services/recipe.service';
import { Patients } from './../../models/patients';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientsService } from './../../services/patients.service';
import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {
  patients: Patients = new Patients();
  photo: any;
  datos:any;
  recipe: Recipe = new Recipe();
  constructor(private patient: PatientsService, private router: ActivatedRoute, private route: Router,
    private recipes: RecipeService) { }

  ngOnInit() {
    const uuid = this.router.snapshot.paramMap.get('uuid');
    if (uuid !== 'nuevo') {
      this.patient.edit(uuid).pipe(
        map((resp: any) => {
          this.patients = resp;
          console.log(this.patients);
          return this.patients
        })
      ).subscribe();
    }
    this.recipes.list().subscribe(
      res =>{
        this.datos = res;
        console.log(this.datos);
      }
    );

  }
  returnReceta(variable) {
    this.recipe.uuid = variable;
    console.log(this.recipe.uuid);
    this.recipes.returnRecipe(this.recipe.uuid);
  }
  DownloadPdf(varia) {
    this.recipe.uuid = varia;
    console.log(this.recipe.uuid);
    this.recipes.DownloadPdf(this.recipe.uuid);
  }
  guardar(from: NgForm) {
    if (from.invalid) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Ha ocurrido un error',
        text: 'Favor de verificar los datos ingresados',
        timer: 1500,
      });
      console.log('Formulario no valido');
      return;
    }
    console.log(this.recipe);
    if (this.recipe.uuid) {
      this.recipes.Update(this.recipe).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se actualizo correctament',
            showConfirmButton: false,
            timer: 1500,
          });
        });
    }
    else {
      this.recipes.insert(this.recipe).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El hospital se registro correctamente',
            showConfirmButton: false,
            timer: 1500,
          });
        });
    }
    from.reset();
    this.ngOnInit();
  }
  returnInquirie(inquiries_id){
    this.recipe.inquiries_id = inquiries_id;
    console.log(this.recipe.inquiries_id);

  }
  borrar(varia) {
    Swal.fire({
      title: '¿Estas seguro que quieres Eliminar este dato?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: 'success',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.recipe.uuid = varia;
        console.log(this.recipe.uuid );
        this.recipes.delete(this.recipe.uuid).subscribe(
          resp => {
            console.log(resp);
            this.ngOnInit();

          }
        )
      }
    })
  }
  editRecipes(variable) {
    this.recipe.uuid = variable;
    this.recipes.edit(this.recipe.uuid).pipe(
      map((resp: any) => {
        this.recipe = resp;
        console.log(this.recipe);
        return this.recipe;
      })
    ).subscribe();
  }
}
