import { InquiriesService } from './../../services/inquiries.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HospitalesService } from 'src/app/services/hospitales.service';
import { DoctorsService } from 'src/app/services/doctors.service';
import { PatientsService } from './../../services/patients.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  cargando: boolean = false;
  patients:any;
  hospitals:any;
  CountPatient:any;
  Expedient:any;
  countDoctor:any;
  constructor(private router: Router, private inquieriesService: InquiriesService,
    private hospital: HospitalesService, private CountPatients: PatientsService,
    private CountDoctors: DoctorsService) { }

  ngOnInit(){
    this.ListPatients();
    this.CountHospitals();
    this.Count_Patients();
    this.Count_Expedientes();
    this.Count_doctors();
  }
  ListPatients(){
    this.cargando = true;

    this.inquieriesService.list().subscribe(
      resp => {
        this.patients = resp;
        this.cargando = false;
      }
    )
  }
  CountHospitals(){
    this.hospital.CountHospitals().subscribe(
      resp =>{
        this.hospitals = resp;
      }
    );
  }
  Count_Patients(){
    this.CountPatients.PatienTratamient().subscribe(
      resp =>{
          this.CountPatient = resp;
      }
    );
  }
  Count_Expedientes(){
    this.CountPatients.Count_Expedients().subscribe(
      resp => {
        this.Expedient = resp;
      }
    );
  }
  Count_doctors(){
    this.CountDoctors.Doctors_Count().subscribe(
      resp =>{
        this.countDoctor = resp;
      }
    );
  }
}
