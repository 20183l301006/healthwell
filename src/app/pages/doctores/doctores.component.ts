import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DoctorsService } from 'src/app/services/doctors.service';
import Swal from 'sweetalert2';
import { Doctors } from 'src/app/models/doctors';
import { HospitalesService } from 'src/app/services/hospitales.service';

@Component({
  selector: 'app-doctores',
  templateUrl: './doctores.component.html',
  styleUrls: ['./doctores.component.css']
})
export class DoctoresComponent implements OnInit {
doctors: any;
doctors2: any;

cargando: boolean = false;
cargando2: boolean = false;

dato: boolean = false;
registro: Doctors = new Doctors();
hospitales: any;
  constructor(private docttorsService: DoctorsService, private hospitalService:HospitalesService) {}

  ngOnInit(){
    this.listDoctors();
    this.listHospital();
    this.listDoctors_bloquers();

  }
listDoctors(){
  this.cargando = true;
  this.docttorsService.list().subscribe(
    resp => {
      this.doctors = resp;
      this.cargando = false;
    }
  );
}
listDoctors_bloquers(){
  this.cargando2 = true;
  this.docttorsService.list_Bloquers().subscribe(
    resp => {
      this.doctors2 = resp;
      this.cargando2 = false;
    }
  );
}
listHospital(){
  this.hospitalService.list().subscribe(
    resp =>{
      this.hospitales = resp;
    }
  );
}
clickTrue(event){
  this.dato = event;
}
clickFalse(){
  this.dato = false;
}
guardar(from: NgForm) {
  if (from.invalid) {
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: 'Ha ocurrido un error',
      text: 'Favor de verificar los datos ingresados',
      timer: 1500,
    });
    console.log('Formulario no valido');
    return;
  }
  if (this.registro.uuid) {
    this.docttorsService.Update(this.registro).subscribe(
      resp => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Se actualizo correctament',
          showConfirmButton: false,
          timer: 1500,
        });
        this.listDoctors();
        from.reset();
      });
  }
  else {
    this.docttorsService.insert(this.registro).subscribe(
      resp => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'El doctor se ha registrado correctamente',
          showConfirmButton: false,
          timer: 1500,
        });
        this.listDoctors();
        from.reset();

      });
  }    
}

borrar(doctors: Doctors, i: number) {
  Swal.fire({
    title: '¿Estas seguro que quieres bloquear a este usuario?',
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: 'success',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then((result) => {
    if (result.value) {
      console.log(doctors.uuid);
      this.docttorsService.delete(doctors.uuid).subscribe(
        resp => {
          this.doctors.splice(i, 1);
          this.listDoctors_bloquers();

        }
      )
    }
  })
}
desbloquear(doctors2: Doctors, j: number) {
  Swal.fire({
    title: '¿Estas seguro que quieres desbloquear a este usuario?',
    icon: 'info',
    showCancelButton: true,
    confirmButtonColor: 'success',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si',
    cancelButtonText: 'No',
  }).then((result) => {
    if (result.value) {
      console.log(doctors2.uuid);
      this.docttorsService.return_datos(doctors2.uuid).subscribe(
        resp => {
          this.doctors2.splice(j, 1);
          this.listDoctors();

        }
      )
    }
  })
}
}
