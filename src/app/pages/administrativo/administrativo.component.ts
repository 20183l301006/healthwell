import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { Admin } from 'src/app/models/admin';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-administrativo',
  templateUrl: './administrativo.component.html',
  styleUrls: ['./administrativo.component.css']
})
export class AdministrativoComponent implements OnInit {
cargando: boolean = false;
admins: any;  
registro: Admin = new Admin();
constructor(private AdminsController: AdminService) { }

  ngOnInit(): void {
    this.list()
  }

  list(){
    this.cargando = true;

    this.AdminsController.list().subscribe(
      resp =>{
        this.admins = resp;
        this.cargando = false;
        console.log(this.admins)
      }
    );
  }

  borrar(admins: Admin, i: number) {
    Swal.fire({
      title: '¿Estas seguro que quieres Eliminar este dato?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: 'success',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        console.log(admins.uuid);
        this.AdminsController.delete(admins.uuid).subscribe(
          resp => {
            console.log(resp);
            this.admins.splice(i, 1);

          }
        )
      }
    })
  }

  guardar(from: NgForm) {
    if (from.invalid) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Ha ocurrido un error',
        text: 'Favor de verificar los datos ingresados',
        timer: 1500,
      });
      console.log('Formulario no valido');
      return;
    }
    console.log(this.registro);
    if (this.registro.uuid) {
      this.AdminsController.Update(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se actualizo correctament',
            showConfirmButton: false,
            timer: 1500,
          });
          this.list();
          from.reset();
        });
    }
    else {
      this.AdminsController.insert(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El administrador se ha registrado correctamente',
            showConfirmButton: false,
            timer: 1500,
          });
          this.list();
          from.reset();

        });
    }    
  }

}
