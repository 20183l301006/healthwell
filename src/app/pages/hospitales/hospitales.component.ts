import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { HospitalesService } from './../../services/hospitales.service';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Hospitals } from 'src/app/models/hospitales';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { timer } from 'rxjs';
import { LoginService } from 'src/app/services/login.service';
@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styleUrls: ['./hospitales.component.css']
})
export class HospitalesComponent implements OnInit {
  hospitals: any;
  dato: any;
  cargando: boolean = false;
  registro: Hospitals = new Hospitals();
  modal : boolean = false;
  photo: any;
  public afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.jpeg",
    maxSize: "500",
    uploadAPI: {
      url: this.hospital.url + 'hospitals/upload?token='+this.token.leer_token(),
    },
    theme: "attachPin",
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      attachPinBtn: 'Subir logo del hospital',
    }
  };

  constructor(private hospital: HospitalesService, private route: ActivatedRoute,
    private router: Router, private token: LoginService) { }

  ngOnInit() {
   
    this.listarHospitales();
    
  }

  listarHospitales(){
    this.cargando = true;

    this.hospital.list().subscribe(
      res => {
        this.hospitals = res;
        this.cargando = false;
      }
    );
  }



  borrar(hospitals: Hospitals, i: number) {
    Swal.fire({
      title: '¿Estas seguro que quieres Eliminar este dato?',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: 'success',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        console.log(hospitals.uuid);
        this.hospital.delete(hospitals.uuid).subscribe(
          resp => {
            console.log(resp);
            this.hospitals.splice(i, 1);

          }
        )
      }
    })
  }

  guardar(from: NgForm) {
    if (from.invalid) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Ha ocurrido un error',
        text: 'Favor de verificar los datos ingresados',
        timer: 1500,
      });
      console.log('Formulario no valido');
      return;
    }
    console.log(this.registro);
    if (this.registro.uuid) {
      this.hospital.Update(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se actualizo correctament',
            showConfirmButton: false,
            timer: 1500,
          });
          this.listarHospitales();
          from.reset();
        });
    }
    else {
      this.hospital.insert(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El hospital se registro correctamente',
            showConfirmButton: false,
            timer: 1500,
          });
          this.listarHospitales();
          from.reset();

        });
    }    
  }
  imagenupload(datos) {
    this.photo = datos;
    this.registro.photo = this.photo.body.imagen;
  }
  returnHospital(variable) {
    this.registro.uuid = variable;
    console.log(variable);
    this.hospital.edit(this.registro.uuid).pipe(
      map((resp: any) => {
        this.registro = resp;
        //console.log(this.registro);
        return this.registro;
      })
    ).subscribe();
  }
 
  CerrarModal(){
    this.modal = false;
  }
  AbrirModal(){
    this.modal = true;
  }
  clearHospitals() {
    this.registro.name = "";
    this.registro.direction = "";
    this.registro.photo = "";
  }


}
