import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { LoginComponent } from 'src/app/auth/login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {
  token: any;
  name: string;
  dato: boolean = false;
  email: string;

  constructor(private login:LoginService, private router:Router) {
    if (this.login.leer_token !=null) {
      this.name = this.login.leer_nombre();
      this.email= this.login.leer_email();

      this.dato = true;
    }
  }

  ngOnInit(): void {
  }

salir(){
this.login.logout();

}
User(){
  
}

}
