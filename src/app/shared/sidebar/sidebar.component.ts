import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {
  dato: boolean = false;
  menuItems: any[];
  token: any;
  name: string;
  constructor( private sidebarService: SidebarService, private login:LoginService) {
    this.menuItems = sidebarService.menu;

    if (this.login.leer_token !=null) {
      this.name = this.login.leer_nombre();
      this.dato = true;
    }

  }

  ngOnInit(){
  }

}
