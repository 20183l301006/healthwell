import { map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { HospitalesService } from './../../services/hospitales.service';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Hospitals } from 'src/app/models/hospitales';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { timer } from 'rxjs';
import { HospitalesComponent } from 'src/app/pages/hospitales/hospitales.component';
@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.component.html',
  styleUrls: ['./hospital.component.css']
})
export class HospitalComponent implements OnInit {
  hospitals: any;
  dato: any;
  cargando: boolean = false;
  registro: Hospitals = new Hospitals();
@Input() modal : boolean = false;
@ViewChild("nuevo",{ static: true }) nuevo: ElementRef;
  photo: any;
  public afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.jpeg",
    maxSize: "500",
    uploadAPI: {
      url: this.hospital.url + 'hospitals/upload',
    },
    theme: "attachPin",
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      attachPinBtn: 'Subir logo del hospital',
    }
  };
  constructor(private hospital: HospitalesService, private route: ActivatedRoute,
    private router: Router, private hospitales: HospitalesComponent) { }

  ngOnInit() {
    this.cargando = true;

    this.hospital.list().subscribe(
      res => {
        this.hospitals = res;
        this.cargando = false;
        console.log(this.hospitals);
      }
    );

  }

  guardar(from: NgForm) {
    if (from.invalid) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Ha ocurrido un error',
        text: 'Favor de verificar los datos ingresados',
        timer: 1500,
      });
      console.log('Formulario no valido');
      return;
    }
    console.log(this.registro);
    if (this.registro.uuid) {
      this.hospital.Update(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se actualizo correctament',
            showConfirmButton: false,
            timer: 1500,
          });
        });
    }
    else {
      this.hospital.insert(this.registro).subscribe(
        resp => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'El hospital se registro correctamente',
            showConfirmButton: false,
            timer: 1500,
          });
        });
    }
    from.reset();
    this.CerrarModal();
    
  }


  imagenupload(datos) {
    this.photo = datos;
    this.registro.photo = this.photo.body.imagen;
    console.log(this.registro.photo);
  }
  returnHospital(variable) {
    this.registro.uuid = variable;
    this.hospital.edit(this.registro.uuid).pipe(
      map((resp: any) => {
        this.registro = resp;
        console.log(this.registro);
        return this.registro;
      })
    ).subscribe();
  }
  clearHospitals() {
    this.registro.name = "";
    this.registro.direction = "";
    this.registro.photo = "";
  }


  CerrarModal(){
    this.modal = false;
    console.log(this.modal);
  }
  AbrirModal(variable){
    this.modal = variable;
    console.log(this.modal);
  }
}