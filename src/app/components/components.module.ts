import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms'

import { ChartsModule } from 'ng2-charts';

import { IncrementadorComponent } from './incrementador/incrementador.component';
import { DonaComponent } from './dona/dona.component';
import { HospitalComponent } from './hospital/hospital.component';
import { AngularFileUploaderModule } from "angular-file-uploader";



@NgModule({
  declarations: [
    IncrementadorComponent,
    DonaComponent,
    HospitalComponent
  ],
  exports: [
    IncrementadorComponent,
    DonaComponent,
    HospitalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    AngularFileUploaderModule,

  ]
})
export class ComponentsModule { }
