import { AngularFileUploaderModule } from "angular-file-uploader";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Modulos
import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages/pages.module';
import { AuthModule } from './auth/auth.module';

import { AppComponent } from './app.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { Nopagefound2Component } from './nopagefound2/nopagefound2.component';
import { Nopagefound3Component } from './nopagefound3/nopagefound3.component';
import { Nopagesfound3Component } from './nopagesfound3/nopagesfound3.component';
import { Nopagesfound4Component } from './nopagesfound4/nopagesfound4.component';
import { Nopagesfound5Component } from './nopagesfound5/nopagesfound5.component';
import { FiltrarnombrePipe } from './pipes/filtrarnombre.pipe';
//import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxGuardGuard } from "./providers/ngx-guard.guard";
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
} from 'angularx-social-login';





@NgModule({
  declarations: [
    AppComponent,
    NopagefoundComponent,
    Nopagefound2Component,
    Nopagefound3Component,
    Nopagesfound3Component,
    Nopagesfound4Component,
    Nopagesfound5Component,
    FiltrarnombrePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    AuthModule,
    HttpClientModule,
    AngularFileUploaderModule,
    SocialLoginModule
  ],
  providers: [NgxGuardGuard,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '364120555259-f8rmna5kndjl8gtnu4tlmu0o38n8frpl.apps.googleusercontent.com'
            )
          },
        ]
      } as SocialAuthServiceConfig,
    }], 
  bootstrap: [AppComponent]
})
export class AppModule { }
